package com.example.Monsato.dao;

import com.example.Monsato.model.Customer;
import com.example.Monsato.model.Order;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CustomerRepository extends Repository<Customer, Long> {

    List<Customer> findByName(String name);
    Customer findById(Long id);
    List<Customer> findAllByOrdersIsNull();
    List<Customer> findAll();
    Customer save(Customer customer);

    @Transactional
    default void addOrder(Long id, Order order){
        Customer x = findById(id);
        x.setOrders(order);
        save(x);
    }
}