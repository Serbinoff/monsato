package com.example.Monsato.controllers;

import com.example.Monsato.dao.CustomerRepository;
import com.example.Monsato.model.Customer;
import com.example.Monsato.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
@RequestMapping(path="/")
public class MainController {

	@Autowired
	private CustomerRepository repository;

    @GetMapping("/customers")
    public String getCustomerList(Model model) {
			model.addAttribute("allcustomers",  repository.findAll());
        return "customers";
    }

	@GetMapping("/customer")
	public String findByName(Model model, @RequestParam(value="name")  String name) {
		model.addAttribute("customers", repository.findByName(name));
		return "customer";
	}

	@GetMapping("/customer/{customerId}")
	public String findById(Model model, @PathVariable("customerId") Long customerId) {
		model.addAttribute("customers",  repository.findById(customerId));
		return "customer";
	}

	@PostMapping("/customer")
	public Customer addCustomer (@Valid Customer customer, BindingResult bindingResult, @RequestParam String name,
                           @RequestParam String email) {
		if (bindingResult.hasErrors()) {
            return null;
        }
		Customer c = new Customer();
		c.setName(name);
		c.setEmail(email);
		repository.save(c);
		return c;
	}

	@PostMapping("/addorder")
	public Order addOrder (@Valid Order order, BindingResult bindingResult,  @RequestParam String id, @RequestParam double price) {
		if (bindingResult.hasErrors()) {
			return null;
		}
		Long custId = Long.parseLong(id);
		Order o = new Order(price);
		repository.addOrder(custId, o);
		return o;
	}
}
