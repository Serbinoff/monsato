package com.example.Monsato;

import com.example.Monsato.dao.CustomerRepository;
import com.example.Monsato.model.Customer;
import com.example.Monsato.model.Order;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;


import java.util.Date;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class MonsatoApplication {

	private static final Logger log = LoggerFactory.getLogger(MonsatoApplication.class);

	public static void main(String[] args) {

		SpringApplication.run(MonsatoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			// save a couple of customers
			Customer c = new Customer("Jack", "jack1@mail.ru");
			Order o = new Order(10);
			repository.save(c);
			repository.addOrder(1L, o);
			repository.save(new Customer("Jack", "jack2@gmail.com"));
			repository.save(new Customer("David", "palmer@www.com"));


			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.getName().toString());
			}
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Jack'):");
			log.info("--------------------------------------------");
			repository.findByName("Jack").forEach(x -> {
				log.info(x.getName() + " " + x.getEmail());
				x.getOrders().forEach(order -> {
					log.info(x.getName() + " " + order.getCreatedDate() + " " + order.getPrice());
				});
			});
			log.info("");

			// fetch customers by empty order list
			log.info("Customer found with orders = null:");
			log.info("--------------------------------------------");
			repository.findAllByOrdersIsNull().forEach(x -> {
				log.info(x.getName() + " " + x.getEmail());
			});
			log.info("");
		};
	}
}
